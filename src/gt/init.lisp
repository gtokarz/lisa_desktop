(require "jvm")
(require "clos")

;;; MASTER (load "/eclipse/workspace/JLisa/src/org/jlisa/load-lisa-pc.lisp")
(setf CL-USER::lisa-dir "E:/Uczelnia/G-JLisa/src/org/lisa")

;(load "/eclipse/workspace/JLisa/src/org/lisa/misc/defsystem.lisp")
;(load "/eclipse/workspace/JLisa/src/org/lisa/lisa.system")
;(load "/eclipse/workspace/Jlisa/load-lisa/lisa-macros.lisp")

;; HERE IS THE CODE TO USE WHEN IT ALL WORKS ;-)
; (setf file-list ("file1" "file2" "filen")
; (mapcar #'load file-list)

;packages
; MODIFIED exports for ASSERT
(load (concatenate 'string CL-USER::lisa-dir "/src/packages/pkgdecl.lisp"))

(in-package "LISA")

;utils
(load (concatenate 'string CL-USER::lisa-dir "/src/utils/compose.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/utils/utils.lisp"))

;reflect
; MODIFIED: added armedbear features
(load (concatenate 'string CL-USER::lisa-dir "/src/reflect/reflect.lisp"))

;preamble
; changed the code ... it should work now
(load (concatenate 'string CL-USER::lisa-dir "/src/core/preamble.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/conditions.lisp"))

;core20
; modified with accessor
(load (concatenate 'string CL-USER::lisa-dir "/src/core/deffacts.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/fact.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/watches.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/activation.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/strategies.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/context.lisp"))
; modified with accessor
(load (concatenate 'string CL-USER::lisa-dir "/src/core/rule.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/tms-support.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/rete.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/meta.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/binding.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/pattern.lisp"))
; modified with accessor
(load (concatenate 'string CL-USER::lisa-dir "/src/core/token.lisp"))
;; parser OK after
;; commented let on line 141 BEFORE:
;; Unable to compile function LISA::PREPROCESS-LEFT-SIDE defined in non-null lexical environment.
(load (concatenate 'string CL-USER::lisa-dir "/src/core/parser.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/language.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/core/retrieve.lisp"))

;rete compiler
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node-tests.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/shared-node.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/successor.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node-pair.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/terminal-node.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node1.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/join-node.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node2.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node2-not.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node2-test.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/node2-exists.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/tms.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/rete-compiler.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/network-ops.lisp"))
(load (concatenate 'string CL-USER::lisa-dir "/src/rete/reference/network-crawler.lisp"))

(setf LISA::special-elements '(not exists logical))

;debugger
(load (concatenate 'string CL-USER::lisa-dir "/src/debugger/lisa-debugger.lisp"))

;epilogue
; NO
(load (concatenate 'string CL-USER::lisa-dir "/src/core/epilogue.lisp"))


;; -------------------------
; JLisa available after
; this point
;; -------------------------


;; -------------------------
; Tests
;; -------------------------
;(load (concatenate 'string CL-USER::lisa-dir "/misc/mab-clos.lisp"))
;(load "/eclipse/workspace/JLisa/load-lisa/mab-clos.lisp"))
;(load (concatenate 'string CL-USER::lisa-dir "/misc/mab.lisp"))
;(load "/users/beedlem/lisp/lisa/misc/mab-clos.lisp"))
;(load "/users/beedlem/lisp/lisa/misc/set1.lisp"))

; PATERNO
; (load "/eclipse/workspace/JLisa/paterno/paterno.lisp"))
; (load "/eclipse/workspace/JLisa/paterno/bug1.lisp"))
